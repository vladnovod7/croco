module.exports = {
  chainWebpack: (config) => {
    config.module
      .rule('images')
      .use('url-loader')
      .loader('url-loader')
      .tap((options) => {
        options.limit = 10000;
        options.name = '[name].[ext]';
        return options;
      });
  },
  css: {
    loaderOptions: {
      sass: {
        additionalData: '@import "@/assets/styles/variables.scss"; @import "@/assets/styles/reset.scss";',
      },
    },
  },
};
