export { default as AuthForm } from './AuthForm.vue';
export { default as Tabs } from './Tabs.vue';
export { default as PlayersList } from './PlayersList.vue';
export { default as MessagesList } from './MessagesList.vue';
export { default as PickedWordBlock } from './PickedWordBlock.vue';
export { default as WinnerLayout } from './WinnerLayout.vue';
export { default as NotificationsBlock } from './NotificationsBlock.vue';
