export default {
  LOOKING_FOR_PLAYERS: 'looking for players',
  IN_GAME: 'in game',
  GAME_FINISHED: 'game finished',
};
