import { createStore } from 'vuex';
import {
  Players, Messages, Game, Word, Notifications,
} from '@/_entities';

export default createStore({
  modules: {
    Players: Players.store,
    Messages: Messages.store,
    Game: Game.store,
    Word: Word.store,
    Notifications: Notifications.store,
  },
});
