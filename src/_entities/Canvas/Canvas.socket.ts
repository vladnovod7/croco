import io from '@/utils/services/socket';
import { Point } from '@/_interfaces/Canvas';

export const emitNewPoint = (point: Point) => {
  io.emit('new point', point);
};

export const emitDrawStart = (point: Point) => {
  io.emit('draw start', point);
};

export const emitDrawEnd = () => {
  io.emit('draw end');
};

export const emitClear = () => {
  io.emit('clear');
};

export const subscribeOnDrawingUpdate = (cb: Function) => {
  io.on('drawing update', (payload) => cb(payload));
};
