export default {
  actions: {
    UPDATE_GAME_DATA: 'updateGameData',
  },
  getters: {
    GAME_DATA: 'gameData',
  },
};
