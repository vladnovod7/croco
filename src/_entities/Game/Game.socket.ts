import io from '@/utils/services/socket';
import { GameData } from '@/_interfaces/Game';

// eslint-disable-next-line import/prefer-default-export
export const subscribeOnGameUpdate = (cb: Function) => {
  io.emit('get game status');

  io.on('game status update', (data: GameData) => cb(data));
};
