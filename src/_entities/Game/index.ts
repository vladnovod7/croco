import * as socket from './Game.socket';
import store from './Game.store';
import module from './Game.module';

export default {
  socket,
  store,
  module,
};
