import { GameData } from '@/_interfaces/Game';
import { GAME_STATUS } from '@/_constants';

export default {
  state: () => ({
    status: GAME_STATUS.LOOKING_FOR_PLAYERS,
    roomKing: '',
  }),
  mutations: {
    updateGameData(state, data: GameData) {
      state.status = data.status;
      state.roomKing = data.roomKing;
    },
  },
  actions: {
    updateGameData(context, data: GameData) {
      context.commit('updateGameData', data);
    },
  },
  getters: {
    gameData(state): GameData {
      return state;
    },
  },
};
