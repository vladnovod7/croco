export default {
  actions: {
    UPDATE_WORD: 'updateWord',
  },
  getters: {
    WORD: 'word',
  },
};
