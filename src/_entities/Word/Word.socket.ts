import io from '@/utils/services/socket';

export const emitPickNewWord = () => {
  io.emit('get word');
};

export const subscribeOnWordUpdate = (cb: Function) => {
  io.on('update word', (word: string) => cb(word));
};
