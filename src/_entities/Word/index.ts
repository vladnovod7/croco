import * as socket from './Word.socket';
import store from './Word.store';
import module from './Word.module';

export default {
  socket,
  store,
  module,
};
