export default {
  state: () => ({
    word: '',
  }),
  mutations: {
    updateWord(state, word: string) {
      state.word = word;
    },
  },
  actions: {
    updateWord(context, word: string) {
      context.commit('updateWord', word);
    },
  },
  getters: {
    word(state): string {
      return state.word;
    },
  },
};
