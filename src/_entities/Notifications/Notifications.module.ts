export default {
  actions: {
    UPDATE_NOTIFICATIONS_LIST: 'updateNotificationsList',
  },
  getters: {
    NOTIFICATIONS: 'notifications',
  },
};
