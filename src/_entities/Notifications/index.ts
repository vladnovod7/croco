import store from './Notifications.store';
import module from './Notifications.module';

export default {
  store,
  module,
};
