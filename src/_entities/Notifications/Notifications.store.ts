import { NotificationList } from '@/_interfaces/Notifications';

export default {
  state: () => ({
    notifications: [],
  }),
  mutations: {
    updateNotificationsList(state, list: NotificationList) {
      state.notifications = list;
    },
  },
  actions: {
    updateNotificationsList(context, list: NotificationList) {
      context.commit('updateNotificationsList', list);
    },
  },
  getters: {
    notifications(state): NotificationList {
      return state.notifications;
    },
  },
};
