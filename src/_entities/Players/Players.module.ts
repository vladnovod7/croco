export default {
  actions: {
    UPDATE_PLAYERS_LIST: 'updatePlayersList',
  },
  getters: {
    PLAYERS_LIST: 'playersList',
  },
};
