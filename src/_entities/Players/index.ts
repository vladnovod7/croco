import * as socket from './Players.socket';
import store from './Players.store';
import module from './Players.module';

export default {
  socket,
  store,
  module,
};
