import io from '@/utils/services/socket';
import { Player, PlayersList } from '@/_interfaces/Players';

export const emitNewPlayer = (user: Player) => {
  io.emit('new user', user);
};

export function subscribeOnPlayerListUpdate(cb: Function) {
  io.on('userlist update', (list: PlayersList) => cb(list));
}
