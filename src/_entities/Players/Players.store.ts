import { PlayersList } from '@/_interfaces/Players';

export default {
  state: () => ({
    players: [],
  }),
  mutations: {
    updatePlayersList(state, list: PlayersList) {
      state.players = list;
    },
  },
  actions: {
    updatePlayersList(context, list: PlayersList) {
      context.commit('updatePlayersList', list);
    },
  },
  getters: {
    playersList(state): PlayersList {
      return state.players;
    },
  },
};
