import * as socket from './Messages.socket';
import store from './Messages.store';
import module from './Messages.module';

export default {
  socket,
  store,
  module,
};
