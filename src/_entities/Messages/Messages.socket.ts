import io from '@/utils/services/socket';
import { Message, UpdateMessage, MessagesList } from '@/_interfaces/Messages';

export const emitNewMessage = (message: Message) => {
  io.emit('new message', message);
};

export const emitMessageUpdateStatus = (data: UpdateMessage) => {
  io.emit('update message', data);
};

export const subscribeOnMessagesListUpdate = (cb: Function) => {
  io.emit('get messages list');

  io.on('messages list update', (list: MessagesList) => cb(list));
};
