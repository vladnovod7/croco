import { MessagesList } from '@/_interfaces/Messages';

export default {
  state: () => ({
    messages: [],
  }),
  mutations: {
    updateMessagesList(state, list: MessagesList) {
      state.messages = list;
    },
  },
  actions: {
    updateMessagesList(context, list: MessagesList) {
      context.commit('updateMessagesList', list);
    },
  },
  getters: {
    messagesList(state): MessagesList {
      return state.messages;
    },
  },
};
