export default {
  actions: {
    UPDATE_MESSAGES_LIST: 'updateMessagesList',
  },
  getters: {
    MESSAGES_LIST: 'messagesList',
  },
};
