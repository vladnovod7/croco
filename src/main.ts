import { createApp } from 'vue';
import * as firebase from 'firebase';
import { FIREBASE_CONFIG } from '@/_constants';
import App from './App.vue';
import router from './router';
import store from './store';

firebase.initializeApp(FIREBASE_CONFIG);

createApp(App)
  .use(store)
  .use(router)
  .mount('#app');
