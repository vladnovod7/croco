import * as firebase from 'firebase';
import { onMounted } from 'vue';
import router from '@/router';

export default () => {
  const user = firebase.auth().currentUser;

  onMounted(() => {
    firebase.auth().onAuthStateChanged((userData) => {
      if (userData) {
        router.push('/');
      } else {
        router.push('/auth');
      }
    });
  });

  if (!user) {
    router.push('/auth');
  }

  return {
    user: user || null,
  };
};
