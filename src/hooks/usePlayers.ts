import {
  reactive, onMounted,
  computed, toRefs,
} from 'vue';
import * as firebase from 'firebase';
import { useStore } from 'vuex';
import { Players } from '@/_entities';

const { emitNewPlayer, subscribeOnPlayerListUpdate } = Players.socket;
const { UPDATE_PLAYERS_LIST } = Players.module.actions;
const { PLAYERS_LIST } = Players.module.getters;

export default function usePlayers() {
  const { dispatch, getters } = useStore();
  const state = reactive({
    playersList: computed(() => getters[PLAYERS_LIST]),
  });

  const handleUpdatePlayersList = (list) => {
    dispatch(UPDATE_PLAYERS_LIST, list);
  };

  onMounted(() => {
    subscribeOnPlayerListUpdate(handleUpdatePlayersList);

    const user = firebase.auth().currentUser;

    if (!user) return;

    emitNewPlayer({
      id: user.uid,
      email: user.email,
    });
  });

  return {
    ...toRefs(state),
  };
}
