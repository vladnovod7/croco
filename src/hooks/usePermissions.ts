import * as firebase from 'firebase';
import { reactive, computed, toRefs } from 'vue';
import { useStore } from 'vuex';
import { Game } from '@/_entities';

const { GAME_DATA } = Game.module.getters;

export default function usePermissions(playerId = null) {
  const user = firebase.auth().currentUser;

  const userId = playerId || user?.uid;

  const { getters } = useStore();

  const state = reactive({
    gameData: computed(() => getters[GAME_DATA]),
    isKing: computed(() => state.gameData.roomKing === userId),
  });
  return {
    ...toRefs(state),
  };
}
