import {
  onMounted, computed, reactive, toRefs,
} from 'vue';
import { useStore } from 'vuex';
import { GAME_STATUS } from '@/_constants';
import { Game } from '@/_entities';

const { subscribeOnGameUpdate } = Game.socket;
const { UPDATE_GAME_DATA } = Game.module.actions;
const { GAME_DATA } = Game.module.getters;

export default function useGameState() {
  const { dispatch, getters } = useStore();

  const state = reactive({
    gameData: computed(() => getters[GAME_DATA]),
    isLookingForPlayers: computed(() => GAME_STATUS.LOOKING_FOR_PLAYERS === state.gameData.status),
    isInGame: computed(() => GAME_STATUS.IN_GAME === state.gameData.status),
    isGameFinished: computed(() => GAME_STATUS.GAME_FINISHED === state.gameData.status),
  });

  const handleGameUpdate = (data) => {
    dispatch(UPDATE_GAME_DATA, data);
  };

  onMounted(() => {
    subscribeOnGameUpdate(handleGameUpdate);
  });

  return {
    ...toRefs(state),
  };
}
