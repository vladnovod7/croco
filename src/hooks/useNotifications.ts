import { uniqueId } from 'lodash';
import {
  reactive, toRefs, computed,
} from 'vue';
import { useStore } from 'vuex';
import { Notifications } from '@/_entities';
import { Notification } from '@/_interfaces/Notifications';

const { UPDATE_NOTIFICATIONS_LIST } = Notifications.module.actions;
const { NOTIFICATIONS } = Notifications.module.getters;

export default () => {
  const { dispatch, getters } = useStore();

  const state = reactive({
    notifications: computed(() => getters[NOTIFICATIONS]),
  });

  const removeNotifications = (notificationId: Notification['id']) => {
    dispatch(
      UPDATE_NOTIFICATIONS_LIST,
      state.notifications.filter(({ id }) => notificationId !== id),
    );
  };

  const setNotification = (notification: Notification) => {
    dispatch(
      UPDATE_NOTIFICATIONS_LIST,
      [...state.notifications, notification],
    );

    setTimeout(() => {
      removeNotifications(notification.id);
    }, 5000);
  };

  function notify(
    content: Notification['content'],
    type: Notification['type'],
  ) {
    setNotification({
      id: uniqueId(),
      content,
      type,
    });
  }

  return {
    ...toRefs(state),
    notify: {
      notify,
      error: (content) => notify(content, 'error'),
      success: (content) => notify(content, 'success'),
    },
  };
};
