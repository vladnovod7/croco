import { throttle } from 'lodash';
import { ref, onMounted } from 'vue';
import { Canvas } from '@/_entities';

const {
  emitNewPoint, emitDrawStart, emitDrawEnd, emitClear, subscribeOnDrawingUpdate,
} = Canvas.socket;

const windowSizes = {
  height: window.innerHeight,
  width: window.innerWidth - 350,
};

export default function useCanvas() {
  const canvasElement = ref();
  const canvasCtx = ref();
  const isDrawing = ref(false);

  const getPercentageFromCoordinate = (value, key) => (100 * value) / windowSizes[key];
  const getCoordinateFromPercentage = (value, key) => (windowSizes[key] * value) / 100;

  const handleMouseMove = throttle(({ offsetX, offsetY }) => {
    if (!isDrawing.value) return;

    emitNewPoint({
      x: getPercentageFromCoordinate(offsetX, 'width'),
      y: getPercentageFromCoordinate(offsetY, 'height'),
    });
  }, 50);

  const handleMouseDown = ({ offsetX, offsetY }) => {
    isDrawing.value = true;

    emitDrawStart({
      x: getPercentageFromCoordinate(offsetX, 'width'),
      y: getPercentageFromCoordinate(offsetY, 'height'),
    });
  };

  const handleMouseUp = () => {
    isDrawing.value = false;
    emitDrawEnd();
  };

  const handleClear = () => {
    emitClear();
    handleMouseUp();
  };

  const handleDrawingChange = ({ type, point }) => {
    switch (type) {
      case 'drawStart': {
        canvasCtx.value.moveTo(
          getCoordinateFromPercentage(point.x, 'width'),
          getCoordinateFromPercentage(point.y, 'height'),
        );
        break;
      }
      case 'newPoint': {
        canvasCtx.value.lineTo(
          getCoordinateFromPercentage(point.x, 'width'),
          getCoordinateFromPercentage(point.y, 'height'),
        );
        canvasCtx.value.moveTo(
          getCoordinateFromPercentage(point.x, 'width'),
          getCoordinateFromPercentage(point.y, 'height'),
        );
        canvasCtx.value.stroke();
        break;
      }
      case 'drawEnd': {
        isDrawing.value = false;
        break;
      }
      case 'clear': {
        canvasCtx.value.clearRect(0, 0, windowSizes.width, windowSizes.height);
        canvasCtx.value.beginPath();
        break;
      }
      default: {
        isDrawing.value = false;
      }
    }
  };

  onMounted(() => {
    canvasElement.value.height = windowSizes.height;
    canvasElement.value.width = windowSizes.width;
    canvasCtx.value = canvasElement.value.getContext('2d');

    canvasCtx.value.beginPath();
    canvasCtx.value.lineWidth = 5;
    canvasCtx.value.lineCap = 'round';
    canvasCtx.value.shadowColor = 'rgba(0,0,0,.5)';
    canvasCtx.value.shadowBlur = 2;

    subscribeOnDrawingUpdate(handleDrawingChange);
  });

  return {
    canvasElement,
    handleMouseDown,
    handleMouseMove,
    handleMouseUp,
    handleClear,
  };
}
