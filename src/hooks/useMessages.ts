import { v4 } from 'uuid';
import { firebase } from '@/utils/services';
import { useStore } from 'vuex';
import { Messages } from '@/_entities';
import { MessagesList } from '@/_interfaces/Messages';
import {
  reactive, toRefs, onMounted, computed,
} from 'vue';

const { emitNewMessage, emitMessageUpdateStatus, subscribeOnMessagesListUpdate } = Messages.socket;
const { UPDATE_MESSAGES_LIST } = Messages.module.actions;
const { MESSAGES_LIST } = Messages.module.getters;

export default function useMessages() {
  const user = firebase.getUserData();
  const { dispatch, getters } = useStore();

  const state = reactive({
    inputtedValue: '',
    messagesList: computed(() => getters[MESSAGES_LIST]),
  });

  const emitNew = () => {
    emitNewMessage({
      id: v4(),
      email: user.email,
      text: state.inputtedValue,
      status: 'pending',
    });

    state.inputtedValue = '';
  };

  const emitUpdate = (id, status) => {
    emitMessageUpdateStatus({
      id,
      status,
    });

    state.inputtedValue = '';
  };

  const handleUpdateMessagesList = (list: MessagesList) => {
    dispatch(UPDATE_MESSAGES_LIST, list);
  };

  onMounted(() => {
    subscribeOnMessagesListUpdate(handleUpdateMessagesList);
  });

  return {
    ...toRefs(state),
    emitNew,
    emitUpdate,
  };
}
