import { useStore } from 'vuex';
import { Word } from '@/_entities';
import {
  reactive, toRefs, onMounted, computed,
} from 'vue';

const { emitPickNewWord, subscribeOnWordUpdate } = Word.socket;
const { UPDATE_WORD } = Word.module.actions;
const { WORD } = Word.module.getters;

export default function usePickedWord() {
  const { dispatch, getters } = useStore();

  const state = reactive({
    word: computed(() => getters[WORD]),
  });

  const emit = () => {
    emitPickNewWord();
  };

  const handleUpdateWord = (word: string) => {
    dispatch(UPDATE_WORD, word);
  };

  onMounted(() => {
    subscribeOnWordUpdate(handleUpdateWord);
  });

  return {
    ...toRefs(state),
    emit,
  };
}
