export interface Point {
  x: number;
  y: number;
}

export interface Line {
  from: Point;
  to: Point;
}
