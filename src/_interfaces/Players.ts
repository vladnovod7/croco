export interface Player {
  id: string;
  email: string | null;
}

export type PlayersList = Array<Player>;
