export type NotificationTypes = 'error' | 'success'

export interface Notification {
  id: string;
  content: string;
  type: NotificationTypes;
}

export type NotificationList = Notification[];
