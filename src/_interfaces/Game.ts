export type GameStatus = 'looking for players' | 'in game' | 'game finished'

export interface GameData {
  status: GameData;
  roomKing: string;
}
