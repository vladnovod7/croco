export type MessageStatus = 'like' | 'dislike' | 'pending';

export interface Message {
  id: string;
  email: string | null;
  text: string;
  status: MessageStatus;
}

export interface UpdateMessage {
  id: string;
  status: MessageStatus;
}

export type MessagesList = Array<Message>;
