// eslint-disable-next-line import/prefer-default-export
export { default as firebase } from './firebase';
export { default as socketio } from './socket';
