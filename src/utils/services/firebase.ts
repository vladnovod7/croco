import firebase from 'firebase';

export const signUpNewUser = (email: string, password: string) => firebase
  .auth()
  .createUserWithEmailAndPassword(email, password);

export const logIn = (email: string, password: string) => firebase
  .auth()
  .signInWithEmailAndPassword(email, password);

export const getUserData = (): Record<string, any> => {
  const user = firebase.auth().currentUser;

  return user || {};
};

export default {
  signUpNewUser,
  logIn,
  getUserData,
};
